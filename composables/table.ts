import table from './table.json'

export function useTable() {
  const tableState = useState('table', () => table)

  return {
    tableState,
  }
}
